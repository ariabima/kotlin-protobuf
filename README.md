# Kotlin Protobuf Code Generator Output Example

This is a sample of generated protobuf Kotlin class using streem/pbandk library. To see the generated protobuf class in action, simply run the tests inside the `ProtobufTest` class.

## Generating the class from protofile
Before generating the kotlin classes, you need to download the latest protoc build from Google Protocol Buffer
repository. Next, download the latest source code from pbandk repository, build the jar file and rename it to
`protoc-gen-kotlin` (remove the .jar extension as well); set it to runnable jar with chmod +x; the put it in the
same path as the protoc file and set the PATH to it. Finally, generate the class using this command
```
protoc --kotlin_out=out TheNameOfYourProtoFile.proto
```
More details on using protoc can be found in Google Protocol Buffer documentation

## Running the generated class

Copy the generate class to the project, then add this line the app build.gradle dependencies:

```
implementation("pro.streem.pbandk:pbandk-runtime-jvm:0.9.0")
```

Also you might need to add multidex support in case you have a lot of functions generated from you proto file.

## Several notes & Gotchas

* `option java_outer_classname` and `option java_multiple_files` doesn't seems to work. Can be configuration settings issue, but most likely not supported yet by pbandk.
* pbandk defaults the name of the generated kotlin file to `YourProtoFileName.kt`, including all the cases.
* pbandk runtime requires opt-in confirmation in the code (automatically added in the generated kotlin class as `@file:OptIn`). This will require compiler to add `-Xopt-in` to its arguments. In this example code, we add this line to the project's build.gradle file:

```
tasks.withType(org.jetbrains.kotlin.gradle.tasks.KotlinCompile).all {
        kotlinOptions {
            freeCompilerArgs += '-Xopt-in=kotlin.RequiresOptIn'
        }
    }
```
* pbandk seems to also uses kotlinx serialization; to avoid duplicate issues when building if you use it somewhere else, add this on the app build.gradle file:

```
 packagingOptions {
        exclude("META-INF/*.kotlin_module")
    }
```