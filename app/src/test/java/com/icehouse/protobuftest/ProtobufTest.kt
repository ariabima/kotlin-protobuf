package com.icehouse.protobuftest

import org.junit.Assert.assertEquals
import org.junit.Test
import pbandk.decodeFromByteArray
import pbandk.encodeToByteArray

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ProtobufTest {

    @Test
    fun `test write and read person data`() {
        val expected = Person(
            name = "Test Name",
            id = 1,
            email = "lala@lala.com"
        )
        val person = Person(
            name = expected.name,
            id = expected.id,
            email = expected.email
        )

        val byteData = person.encodeToByteArray()
        val result = Person.decodeFromByteArray(byteData)

        assertEquals("Message matched", expected, result)
    }

    @Test
    fun `test write and read person data with phone numbers`() {
        val expectedPhoneNumbers = listOf(
            Person.PhoneNumber(
                number = "081111111",
                type = Person.PhoneType.HOME
            ),
            Person.PhoneNumber(
                number = "081111112",
                type = Person.PhoneType.WORK
            )
        )

        val expected = Person(
            name = "Test Name",
            id = 1,
            email = "lala@lala.com",
            phones = expectedPhoneNumbers
        )
        val person = Person(
            name = expected.name,
            id = expected.id,
            email = expected.email,
            phones = expected.phones
        )

        val byteData = person.encodeToByteArray()
        val result = Person.decodeFromByteArray(byteData)

        assertEquals("Message matched", expected, result)
    }
}